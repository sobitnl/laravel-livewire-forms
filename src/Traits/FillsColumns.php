<?php

namespace SobitNL\LaravelLivewireForms\Traits;

use Illuminate\Support\Facades\Schema;

trait FillsColumns
{
    public function getFillable()
    {
        return Schema::getColumnListing($this->getTable());
    }
}
